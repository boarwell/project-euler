let rec f i sum =
  if i = 999 then sum
  else
  if i mod 3 = 0 || i mod 5 = 0 then f (i+1) (sum+i)
  else f (i+1) sum

let () = Printf.printf "%d\n" (f 1 0)