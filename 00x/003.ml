let n = 600851475143

let rec f i x =
  match x with
  | 1 -> i
  | x' when x' mod i = 0 -> f i (x / i)
  | _ -> f (i + 1) x

let () = Printf.printf "%d\n" (f 2 n)