let is_palindrome x =
  let s = string_of_int x in
  let len = String.length s in
  (*このアルゴリズムはなんかのソートでやった*)
  let rec aux i j =
    if j < i then true
    else if s.[i] = s.[j] then aux (i + 1) (j - 1)
    else false
  in
  aux 0 (len - 1)


let ls = List.init 999 (fun x -> 999 - x)

let rec solve x tmp =
  if x = 1 then tmp
  else
    let ls' = List.map (( * ) x) ls in
    let tmp' = try List.find is_palindrome ls' with _ -> min_int in
    if tmp < tmp' then solve (x - 1) tmp' else solve (x - 1) tmp


let () = solve 999 min_int |> Printf.printf "%d\n"
