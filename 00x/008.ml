let chr2int c = (int_of_char c ) - 48

let ar = Array.init 1000 (fun _ -> Scanf.scanf "%c" chr2int)

let twelfth_multiple i =
  let rec aux i' tmp =
    if i' > (i + 12) then tmp else aux (i' + 1) (tmp * ar.(i'))
  in
  aux i 1

let rec main i ans =
  if i > 987 (*1000 - 13*) then ans
  else
    let tmp = twelfth_multiple i in
    if ans < tmp then main (i + 1) tmp else main (i + 1) ans

let () = main 0 min_int |> Printf.printf "%d\n"