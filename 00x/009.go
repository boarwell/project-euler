package main

import "fmt"

func main() {
	for a := 1; a <= 333; a++ {
		for b := a + 1; a+b <= 666; b++ {
			c := 1000 - a - b
			if !(a < b && b < c) {
				continue
			}
			if a*a+b*b == c*c {
				fmt.Println(a * b * c)
			}
		}
	}
}
