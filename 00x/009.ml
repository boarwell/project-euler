(*もし見つからなければ無限ループになります*)
let inner a =
  let rec aux b =
    if a + b >= 666 then None
    else
      let c = 1000 - a - b in
      if a * a + b * b = c * c then Some (a * b * c) else aux (b + 1)
  in
  aux (a + 1)

let rec outer a =
  match inner a with
  | None -> outer (a + 1)
  | Some x -> x

let () = outer 1 |> Printf.printf "%d\n"