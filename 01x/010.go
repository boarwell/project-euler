package main

import (
	"fmt"
	"math"
)

func main() {
	sum := 2
	for i := 3; i < 2000000; i = i + 2 {
		if isPrime(i) {
			sum += i
		}
	}

	fmt.Println(sum)
}

func isPrime(n int) bool {
	if n == 2 {
		return true
	} else if n%2 == 0 {
		return false
	}

	root := int(math.Floor(math.Sqrt(float64(n)))) + 1

	for i := 3; i < root; i = i + 2 {
		if n%i == 0 {
			return false
		}
	}

	return true
}
