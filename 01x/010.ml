let is_prime n =
  match n with
  | 1 -> false
  | 2 -> true
  | x when x mod 2 = 0 -> false
  | n ->
    let root = int_of_float (float n |> sqrt) in
    let rec aux i =
      if i > root then true
      else if n mod i = 0 then false
      else aux (i + 2)
    in
    aux 3

(*let primes n = List.init n (fun x -> x) |> List.filter is_prime

  let () = List.fold_left (+) 0 (primes 2000000) |> Printf.printf "%d\n"*)

let rec solve n sum =
  if n > 2000000 then sum
  else if is_prime n then solve (n + 2) (sum + n)
  else solve (n + 2) sum

let () = solve 3 2 |> Printf.printf "%d\n"
